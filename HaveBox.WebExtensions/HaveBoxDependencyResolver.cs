﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;

namespace HaveBox.WebExtensions
{
    public class HaveBoxDependencyResolver : IDependencyResolver
    {
        private IContainer _container;

        public HaveBoxDependencyResolver(IContainer haveBoxContainer)
        {
            _container = haveBoxContainer;
        }

        public IDependencyScope BeginScope()
        {
            return new HaveBoxDependencyResolver(_container.SpawnContainer());
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return _container.GetInstance(serviceType);
            }
            catch(Exception)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return (IEnumerable<object>)_container.GetInstance(typeof(IEnumerable<>).MakeGenericType(serviceType));
            }
            catch (Exception)
            {
                return Enumerable.Empty<object>();
            }
        }

        public void Dispose()
        {
            var disposeableContainer = _container as IDisposableContainer;

            if (disposeableContainer != null)
            {
                disposeableContainer.Dispose();
            }
        }

        public IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return GetServices(serviceType);
        }

        public object GetInstance(Type serviceType)
        {
            return GetService(serviceType);
        }
    }
}
