﻿using FluentAssertions;
using HaveBox.WebExtensions;
using NSubstitute;
using System.Linq;
using System.Web.Http.Dependencies;
using Xunit;

namespace HaveBox.WebExtensionsTests
{
    public class HaveBoxDependencyResolverTests
    {
        private IContainer _container;
        private IDependencyResolver _HaveBoxDependencyResolver;

        public HaveBoxDependencyResolverTests()
        {
            _container = new Container();
            _container.Configure(x => x.For<IValid>().Use<Valid>());
            _HaveBoxDependencyResolver = new HaveBoxDependencyResolver(_container);
        }

        public interface IInvalid
        { }

        public interface IValid
        { }

        public class Valid : IValid
        { }

        [Fact]
        public void Given_A_Container_When_Getting_A_Valid_Instance_Then_Instance_Is_Returned()
        {
            var instance = _HaveBoxDependencyResolver.GetService(typeof(IValid));

            instance.Should().BeOfType<Valid>();
        }

        [Fact]
        public void Given_A_Container_When_Getting_A_Invalid_Instance_Then_Instance_Is_Returned()
        {
            var instance = _HaveBoxDependencyResolver.GetService(typeof(IInvalid));

            instance.Should().BeNull();
        }

        [Fact]
        public void Given_A_Container_When_Getting_A_Valid_Instances_Then_Instance_Is_Returned()
        {
            var instance = _HaveBoxDependencyResolver.GetServices(typeof(IValid));

            instance.First().Should().BeOfType<Valid>();
        }

        [Fact]
        public void Given_A_Container_When_Getting_A_Invalid_Instances_Then_Instance_Is_Returned()
        {
            var instance = _HaveBoxDependencyResolver.GetServices(typeof(IInvalid));

            instance.Should().BeEmpty();
        }

        [Fact]
        public void Given_A_Container_When_Getting_A_Beginning_Scope_Then_A_Container_Is_Spawned()
        {
            var container = Substitute.For<IContainer>();
            var HaveBoxDependencyResolver = new HaveBoxDependencyResolver(container);

            HaveBoxDependencyResolver.BeginScope();

            container.Received(1).SpawnContainer();
        }

        [Fact]
        public void Given_A_Container_When_Disposing_Scope_Then_A_Container_Is_Spawned()
        {
            var container = Substitute.For<IDisposableContainer>();
            var HaveBoxDependencyResolver = new HaveBoxDependencyResolver(container);

            HaveBoxDependencyResolver.Dispose();

            container.Received(1).Dispose();
        }
    }
}
